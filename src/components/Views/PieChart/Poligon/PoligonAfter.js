import React, {useEffect, useState} from 'react';
import getRandomColor from '../../../../utils/getRandomColor/getRandomColor';
import PropTypes from 'prop-types';

import './Poligon.scss'

const PoligonAfter = ({x, y, degrees, radius}) => {   
    const [pointsAfter, setPointsAfter] = useState(null); 

    useEffect(() => {
        if (x && y) {
            setPointsAfter(createPointAfter(degrees, radius));
        }
    }, [x, y]);

    const createAfterPointX = (radius, degrees) => {
        return (((radius - 10) * Math.cos((degrees) * Math.PI/180)).toFixed() * 1 + radius)
    }

    const createAfterPointY = (radius, degrees) => {
        return (((radius - 10) * Math.sin(degrees * Math.PI/180)).toFixed() * 1 + radius)
    }

    const createInsideAfterPointX = (radius, degrees) => {
        return (((radius + 10) * Math.cos(degrees * Math.PI/180)).toFixed() * 1 + radius*2)
    }

    const createInSideAfterPointY = (radius, degrees) => {
        return (((radius + 10) * Math.sin(degrees * Math.PI/180)).toFixed() * 1 + radius*2)
    }

    const createPointAfter = (degrees, radius) => {
        return (`${createAfterPointX(radius, degrees-2)}px ${createAfterPointY(radius, degrees-2)}px,
                ${inSideAddAfterPoints(degrees, radius, 2)},
                ${outSideAddAfterPoints(degrees, radius)}`);
    }

    const inSideAddAfterPoints = (degrees, radius, addDegrees) => {
        let pointCount = parseInt(degrees/10);
        let arrPoint = '';
        for (let i = 0; i <= pointCount - 1; i++) {
            if (i === 0) {
                arrPoint = `${createInsideAfterPointX(radius/2, (i*10 + addDegrees))}px ${createInSideAfterPointY(radius/2, (i*10 + addDegrees))}px`;
            } else {
                arrPoint = `${createInsideAfterPointX(radius/2, i*10)}px ${createInSideAfterPointY(radius/2, i*10)}px, ` + arrPoint;
            }
        }
        arrPoint = `${createInsideAfterPointX(radius/2, (degrees - addDegrees))}px ${createInSideAfterPointY(radius/2, (degrees - addDegrees))}px, ` + arrPoint
        return arrPoint;
    }

    const outSideAddAfterPoints = (degrees, radius) => {
        let pointCount = parseInt(degrees/5);
        let arrPoint = '';
        for (let i = 0; i < pointCount; i++) {
            if (i + 1 === pointCount) {
                arrPoint += `${createOutSidePointX(radius, radius - 10, i*5)}px ${createOutSidePointY(radius, radius - 10, i*5)}px`;
            } else {
                arrPoint += `${createOutSidePointX(radius, radius - 10, (i*5  + 2))}px ${createOutSidePointY(radius, radius - 10, (i*5  + 2))}px, `;
            }
        }
        return arrPoint;
    }

    const createOutSidePointX = (radius, radiusGGG, degrees) => {
        return ((radiusGGG * Math.cos(degrees * Math.PI/180)).toFixed() * 1 + radius)
    }

    const createOutSidePointY = (radius, radiusGGG, degrees) => {
        return ((radiusGGG * Math.sin(degrees * Math.PI/180)).toFixed() * 1 + radius)
    }
   
    return ( 

            <div className="poligon__after" style={{
                clipPath: `polygon(${pointsAfter})`,
                backgroundColor: `${getRandomColor()}`
                }}>
            </div>
     );
}

PoligonAfter.propTypes = {
};

PoligonAfter.defaultProps = {
};
 
export default PoligonAfter;