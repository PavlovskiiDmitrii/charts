import React, {useEffect, useState} from 'react';
import PoligonAfter from './PoligonAfter';
import PropTypes from 'prop-types';

import './Poligon.scss'

const Poligon = ({percent,
                degrees,
                value,
                radius,
                transform,
                setMenuOpen,
                name,
                setMenuPosition,
                setMenuInfo,
                backgroundColor}) => {

    const [x, setX] = useState(null);
    const [y, setY] = useState(null);
    const [background, setBackgroun] = useState(null);

    const [pointsMain, setPointsMain] = useState(null);

    const CENTER_POINT = '50% 50%';
    const RIGHT_POINT = '100% 50%';
    const BOTTOM_RIGHT_POINT = '100% 100%';
    const BOTTOM_LEFT_POINT = '0% 100%';
    const TOP_LEFT_POINT = '0% 0%';
    const TOP_RIGHT_POINT = '100% 0%';

    useEffect(() => {
        if (degrees, radius) {
            setX(createPointX(radius, degrees));
            setY(createPointY(radius, degrees));
        }
    }, [degrees, radius]);

    useEffect(() => {
        if (x && y) {
            setPointsMain(createPointMain(degrees, x, y));
        }
    }, [x, y]);


    // попробовать сделать без этого костыля
    useEffect(() => {
        setBackgroun(backgroundColor);
    }, []);

    const createPointX = (radius, degrees) => {
        return ((radius * Math.cos(degrees * Math.PI/180)).toFixed() * 1 + radius)
    }

    const createPointY = (radius, degrees) => {
        return ((radius * Math.sin(degrees * Math.PI/180)).toFixed() * 1 + radius)
    }

    const minimalPoints = (x, y) => {
        return `${x}px ${y}px, ${CENTER_POINT}, ${RIGHT_POINT}, ${BOTTOM_RIGHT_POINT}`;
    }

    const createPointMain = (degrees, x, y) => {
        if (degrees <= 90) {
            return `${minimalPoints(x, y)}`;
        }
        if (degrees <= 180 && degrees > 90) {
            return `${minimalPoints(x, y)}, ${BOTTOM_LEFT_POINT}`;
        }
        if (degrees > 180 && degrees <= 270) {
            return `${minimalPoints(x, y)}, ${BOTTOM_LEFT_POINT}, ${TOP_LEFT_POINT}`;
        }
        if (degrees > 270 && degrees <= 360) {
            return `${minimalPoints(x, y)}, ${BOTTOM_LEFT_POINT}, ${TOP_LEFT_POINT}, ${TOP_RIGHT_POINT}`;
        }
    }

    return ( 
        <div className="poligon"
            onMouseMove={(e) => {
                setMenuPosition({'x': e.pageX, 'y': e.pageY});
            }}
            onMouseOut={() => {
                setMenuOpen(false);
                setMenuInfo({})
            }}
            onMouseOver={() => {
                setMenuOpen(true);
                setMenuInfo({'percent': percent, 'name': name, 'value': value, 'bg': background})
            }}
            
            style={{
            clipPath: `polygon(${pointsMain})`,
            transform: `rotateZ(${transform}deg)`,
            backgroundColor: `${background}`}}>
        </div>
     );
}

Poligon.propTypes = {
    persent: PropTypes.number,
    degrees: PropTypes.number,
    value: PropTypes.number,
    radius: PropTypes.number,
    transform: PropTypes.number,
    name: PropTypes.string,
    setMenuInfo: PropTypes.func,
    setMenuOpen: PropTypes.func,
    setMenuPosition: PropTypes.func,
    backgroundColor: PropTypes.string,
};

Poligon.defaultProps = {
};
 
export default Poligon;