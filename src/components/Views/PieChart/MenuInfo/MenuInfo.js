import React from 'react';
import PropTypes from 'prop-types';

import './MenuInfo.scss'

const MenuInfo = ({item, menuOpen, menuPosition, sum}) => {

    const MIXING_TOP = 120;
    const MIXING_LEFT = 20;

    return (
        <div style={{
            top: `${menuPosition.y - MIXING_TOP}px`,
            left: `${menuPosition.x - MIXING_LEFT}px`,
            backgroundColor: item.bg
        }} className={`menuInfo ${menuOpen ? '' : 'menuInfo_none'}`}>
            <div className="menuInfo__item-wrap">
                <div className="menuInfo__item"> {item.name}</div>
                <div className="menuInfo__item"> {item.value} is {item.percent}% of {sum}</div>
            </div>
        </div>
     );
}

MenuInfo.propTypes = {
    item: PropTypes.object,
    menuOpen: PropTypes.bool,
    menuPosition: PropTypes.object,
    sum: PropTypes.number
};

MenuInfo.defaultProps = {
    item: {},
    menuOpen: false,
    menuPosition: {},
    sum: 100
};
 
export default MenuInfo;