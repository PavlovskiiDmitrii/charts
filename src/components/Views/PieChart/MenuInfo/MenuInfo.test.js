import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, render, configure  } from 'enzyme';
import MenuInfo from './MenuInfo';

configure({adapter: new Adapter()});


describe('<MenuInfo />', () => {

  it('should render component ', () => {
    const component = shallow(<MenuInfo text={'test'}/>);
    const wrapper = component.find(".menuInfo");
    expect(wrapper.length).toBe(1);
  });  

  //Snapshot
  it('should render component Snapshot', () => {
    const component = render(<MenuInfo text={'test'}/>);
    expect(component).toMatchSnapshot();
  });
});