import React, {useRef, useEffect, useState} from 'react';
import Poligon from './Poligon/Poligon';
import MenuInfo from './MenuInfo/MenuInfo';
import getRandomColor from '../../../utils/getRandomColor/getRandomColor';
import PropTypes from 'prop-types';

import './PieChart.scss'

const PieChart = ({json, valid, sum}) => {
    const inputEl = useRef(null);
    const [radius, setRadius] = useState(null);
    const [menuInfo, setMenuInfo] = useState({});
    const [menuOpen, setMenuOpen] = useState(false);
    const [menuPosition, setMenuPosition] = useState({});

    useEffect(() => {
        console.log(json)
        setRadius(inputEl.current.getBoundingClientRect().width * 0.7875 / 2);
    }, []);

    return ( 
        <div className='pieChart__wrap' ref={inputEl}>
            <div className="pieChart">
                <div className="pieChart__border">
                
                    {
                        valid ?
                        <React.Fragment>
                            <div className="t55">
                                {
                                    Object.keys(json).map((item, i) => 
                                        <Poligon key={i} percent={json[item].persent}
                                                        degrees={json[item].deg}
                                                        radius={radius}
                                                        name={item}
                                                        value={json[item].value}
                                                        transform={json[item].transform}
                                                        setMenuInfo={setMenuInfo}
                                                        setMenuOpen={setMenuOpen}
                                                        backgroundColor={getRandomColor()}
                                                        setMenuPosition={setMenuPosition}
                                                        />
                                    )
                                }
                            </div>
                            <div className="pieChart__midlPlug"></div>
                            <div className="pieChart__midlPlug-Shadow"></div>
                            <MenuInfo item={menuInfo} menuOpen={menuOpen} menuPosition={menuPosition} sum={sum}/>
                        </React.Fragment>
                        :
                            <div className="pieChart__notValid">
                                Not Validation Json
                            </div>
                    }
                    
                    <div className="pieChart__midleСircle"></div>
                </div>
            </div>
        </div>
     );
}

PieChart.propTypes = {
};

PieChart.defaultProps = {
};
 
export default PieChart;