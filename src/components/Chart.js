import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import DefaultView from './Views/PieChart/PieChart'
import DefaultJson from '../utils/defaultJson/defaultJson'

const Chart = ({view, json}) => {

    const [valid, setValid] = useState(false);
    const [sum, setSum] = useState(0);
    const [preparJson, setPreparationJson] = useState(0);

    useEffect(() => {
        setValid(validJson(json));
    }, []);

    useEffect(() => {
        if (valid === true) {
            setSum(sumValue(json))
        }
    }, [valid]);

    useEffect(() => {
        if (sum > 0) {
            setPreparationJson(preparationJson(json, sum))
        }
    }, [sum]);

    const validJson = (json) => {
        for (var key in json) {
            if (!json[key].value) {
                return false;
            }
        }
        return true;
    };

    const sumValue = (json) => {
        let sum = 0;
        for (var key in json) {
            sum += json[key].value
        }
        return sum;
    };

    const preparationJson = (json, sum) => {
        let lastItemTransform = null;
        for(let item in json ) {
            json[item].persent = ((json[item].value/sum)*100).toFixed(1);
            json[item].deg = +((json[item].persent * 3.6).toFixed(1));
            lastItemTransform ? json[item].transform = lastItemTransform : '' ;
            lastItemTransform = lastItemTransform + json[item].deg;
        }
        return json;
    }

    return ( 
        <React.Fragment>
            {view({json: preparJson, valid:valid, sum:sum})}
        </React.Fragment>
     );
}

Chart.propTypes = {
    //view: PropTypes.element
    // ......................
    //json: PropTypes.json
};

Chart.defaultProps = {
    view: DefaultView,
    json: DefaultJson
};
 
export default Chart;