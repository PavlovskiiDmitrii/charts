import React from 'react';
import Chart from '../Chart';
import PieChart from '../Views/PieChart/PieChart';
import json from '../../../db.json'

import PropTypes from 'prop-types';

import './App.scss'

const App = () => {
    return ( 
        <div>
            <div style={{
                width: '400px',
                height: '400px'
            }}>
                <Chart view={PieChart} json={json}/>
            </div>
        </div>
     );
}

App.propTypes = {
};

App.defaultProps = {
};
 
export default App;